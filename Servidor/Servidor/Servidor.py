import socket

print("servidor")
ser = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
 
ser.bind(("", 8050))
 
while True:
    ser.listen()
 
    cli, addr = ser.accept()
    print("Recibo conexion de la IP: " + str(addr[0]) + " Puerto: " + str(addr[1]))

    while True:

        try:
            recibido = cli.recv(1024)

            print("Mensaje de la conexion de la IP: " + str(addr[0]) + " Puerto: " + str(addr[1]) + ":")
            print(recibido)
    
            msg_toSend=("mensaje recibido ;)")
            cli.send(msg_toSend.encode('ascii'))

        except:
            cli.close()
            print("Conexion cerrada")
            break

ser.close()

print("Conexiones cerradas")
