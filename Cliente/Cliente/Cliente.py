import socket
print("cliente")

host = 'localhost'
port = 8050

host = input("Ingrese ip del servidor(localhost): ")
port = int(input("Ingrese el puerto del servidor(8050): "))

obj = socket.socket()
 
try:
    obj.connect((host, port))
    print("Conectado al servidor")
 
    while True:
        mens = input("Mensaje desde Cliente a Servidor >> ")
    
        if(mens=="close"):
            break
        
        obj.send(mens.encode('ascii'))
        recibido = obj.recv(1024)

        print("Respuesta del servidor:")
        print(recibido)

    obj.close()

    print("Conexión cerrada")
except:
    print("No se pudo conectar al servidor")
    